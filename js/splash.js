$(function() {  
	/* Elements */
	var $mainNav = null;
	var $homeNav = null;
	var $previewNav = null;
	// var $emailNav = null;	
	var $allContent = null;
	var $homeContent = null;
	var $previewContent = null;
	// var $emailContent = null;	
	var $mainTitle = null;
	var $subHeader = null;
	var $contentGroup = null;
	var $linkButton = null;
	var $addButton = null;
	var $mainContent = null;
	var $previewArea = null;
	var $sendButton = null;
	var $copyButton = null;
	var $imageCropper = null;
	var $cropButton = null;
	var $uploadButton = null;
	var $cropControl = null;
	var $teamText = null;
	var $addNewSplashButton = null;

	/* Variables */
	var imageCropper = null;
	var isImageCropped = false;
	var teamArray = [
        { id: "badminton", name: "Badminton Club", image: "badminton.jpg" },
        { id: "bowling", name: "Bowling Club", image: "bowling.jpg" },
        { id: "csr", name: "CSR", image: "csr.jpg" },
        { id: "dance", name: "Dance Club", image: "dance.jpg" },
        { id: "eighl", name: "EIGHL", image: "eighl.jpg" },
        { id: "employee-enrichment", name: "Employee Enrichment Team", image: "employee-enrichment.jpg" },
        { id: "e-sports", name: "E-Sports", image: "e-sports.png" },
        { id: "icba", name: "ICBA", image: "icba.jpg" },
        { id: "icqp", name: "ICQP", image: "icqp.jpg" },
        { id: "literature", name: "Literature Club", image: "literature.jpg" },
        { id: "multisports", name: "Multisports", image: "multisports.png" },
        { id: "musiko", name: "Musiko", image: "musiko.jpg" },
        { id: "photography", name: "Photography Club", image: "photography.jpg" },
        { id: "tabletennis", name: "Table Tennis Club", image: "tabletennis.jpg" },
        { id: "volleyball", name: "Volleyball Club", image: "volleyball.jpg" }
    ];

	/* Events */
	$(document).ready(function(ev) {
		setVariables();
		bindEvents();
		bindCheckboxes();
		loadData();
	});

	/* Functions */
	function bindCheckboxes() {
		var teamCheckboxes = "";
		var len = teamArray.length;
		for(var i = 0; i < len; i++) {
			var team = teamArray[i];

			teamCheckboxes += "<div class='checkbox'>";			
			teamCheckboxes += "<label>";
			teamCheckboxes += "<input id='" + team.id + "-cb' type='checkbox' class='team-cb' value='' team-id='" + team.id + "' team-logo='" + team.image + "'>"
			teamCheckboxes += team.name;
			teamCheckboxes += "</label>"
			teamCheckboxes += "</div>";
		}

		var $teamCheckboxes = $("#team-checkboxes");
		$teamCheckboxes.append(teamCheckboxes);

		var $teamCB = $teamCheckboxes.find(".team-cb");
		$teamCB.change(function(ev) {
			generateSplash();
		});
	}

	function bindEvents() {
		$mainNav.click(function(ev) {
			/* Set active nav class */
			$mainNav.removeClass('active');

			var $this = $(this);
      		$this.addClass('active');

      		/* Show/hide corresponding content */      		
      		var key = $this[0].attributes["content-key"].value;
      		showContent(key);
		});

		$copyButton.click(function(ev) {
			copyElementContents($previewArea);
		});

		$(".form-control").keyup(function(ev) {
			generateSplash();
		});

	 	$cropButton.click(function(ev) {
	 		var self = this;

	 		imageCropper.result({ type: "rawcanvas", format: "jpeg" }).then(function(val) {
	 			// val.setAttribute("crossOrigin", "anonymous");

			    val.setAttribute("id", "splash-canvas");
			    var dataUrl = val.toDataURL();

				uploadDataUrl(dataUrl);			    

				isImageCropped = true;
			});
	 	}); 

	 	$addButton.click(function(ev) {
	 		var id = $(".remove-control").length ? parseInt($(".remove-control")[$(".remove-control").length - 1].attributes["control-id"].value) + 1 : 1;
	 		var ccid = "cc-" + id;

	 		var contentItem = "<br class='" + ccid + "' />";
			contentItem += "<input class='form-control content-header " + ccid + "' placeholder='Optional' value='Content Header'>";
			contentItem += "<textarea class='form-control main-content " + ccid + "' placeholder='Required' rows='3' value='Main Content...'></textarea>";
			contentItem += "<button class='btn btn-danger remove-control " + ccid + "' control-full-id='cc-" + id + "' control-id='" + id + "'>Remove</button>";

	 		$contentGroup.append(contentItem);

	 		$contentHeader = $(".content-header");
			$mainContent = $(".main-content");

	 		unbindEvents();	 		
	 		bindEvents();
	 		generateSplash();
	 	});

	 	$(".remove-control").click(function(ev) {
	 		var cc = $(this)[0].attributes["control-full-id"].value;

	 		$("." + cc).remove();

	 		$contentHeader = $(".content-header");
			$mainContent = $(".main-content");

	 		unbindEvents();	 		
	 		bindEvents();
	 		generateSplash();
	 	});

	 	$("#uploadImage").change(function() {
	 		var fileData = $("#uploadImage").prop("files")[0];
			var formData = new FormData();
			formData.append("image", fileData); 
			
			$.ajax({
				url: "php/upload-image.php",
				type: "POST",
				data:  formData,
				contentType: false,
				cache: false,
				processData:false,
				beforeSend : function() {
					$("#err").fadeOut();
				},
				success: function(data) {
					if(data == 'invalid file') {
						$("#err").html("Invalid File !").fadeIn();
					} else {
						showCropControls();		

						if(imageCropper) imageCropper.destroy();
						$imageCropper.empty();
						$imageCropper.css({
							"padding-top": "0px",
							"background-color": "white"
						});				

						unbindEvents();	 		
				 		bindEvents();
				 		generateSplash();

						var el = document.getElementById('image-cropper');
						imageCropper = new Croppie(el, {
							boundary: { width: 723, height: 341 },
						    viewport: { width: 723, height: 141 },
						    // boundary: { width: 300, height: 300 },
						    showZoomer: false,
						    enableOrientation: true
						});
						var timestamp = new Date();
						imageCropper.bind({
						    url: 'images/uploads/test.png?t=' + timestamp.getTime()
						});						
					}
				},
				error: function(e) 
				{
					$("#err").html(e).fadeIn();
				}          
			});
	 	});

	 	$uploadButton.click(function() {
	 		var fileData = $("#uploadImage").prop("files")[0];
			var formData = new FormData();
			formData.append("image", fileData); 
			
			$.ajax({
				url: "php/upload-image.php",
				type: "POST",
				data:  formData,
				contentType: false,
				cache: false,
				processData:false,
				beforeSend : function() {
					$("#err").fadeOut();
				},
				success: function(data) {
					if(data == 'invalid file') {
						$("#err").html("Invalid File !").fadeIn();
					} else {
						if(imageCropper) imageCropper.destroy();
						$imageCropper.empty();
						$imageCropper.css({
							"padding-top": "0px",
							"background-color": "white"
						});

						var el = document.getElementById('image-cropper');
						imageCropper = new Croppie(el, {
							boundary: { width: 723, height: 341 },
						    viewport: { width: 723, height: 141 },
						    // boundary: { width: 300, height: 300 },
						    showZoomer: false,
						    enableOrientation: true
						});
						var timestamp = new Date();
						imageCropper.bind({
						    url: 'images/uploads/test.png?t=' + timestamp.getTime()
						});						
					}
				},
				error: function(e) 
				{
					$("#err").html(e).fadeIn();
				}          
			});
	 	});

	 	$addNewSplashButton.click(function(ev) {
	 		showContent("home");

	 		resetSplash();
	 	});
	}

	function copyElementContents(el) {
		el = el[0];

		var body = document.body, range, sel;
		if (document.createRange && window.getSelection) {
			range = document.createRange();
			sel = window.getSelection();
			sel.removeAllRanges();
			try {
			range.selectNodeContents(el);
			sel.addRange(range);
			} catch (e) {
			range.selectNode(el);
			sel.addRange(range);
			}
		} else if (body.createTextRange) {
			range = body.createTextRange();
			range.moveToElementText(el);
			range.select();
		}

		document.execCommand("copy");

		window.getSelection().removeAllRanges();
	}

	function downloadCanvas(link, dataUrl, filename) {
		link.href = dataUrl;
		link.download = filename;
	}

	function generateSplash() {
		$previewArea.empty();

		var tableContainerHTML = "<table class='preview-table'>";
		tableContainerHTML += "<tr id='title-block'>";
		tableContainerHTML += "<td class='logo'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "<td class='headers'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "</tr>";

		tableContainerHTML += "<tr id='image-block'>";
		tableContainerHTML += "<td colspan='2'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "</tr>";

		tableContainerHTML += "<tr id='content-block'>";
		tableContainerHTML += "<td colspan='2'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "</tr>";

		tableContainerHTML += "<tr id='team-block'>";
		tableContainerHTML += "<td colspan='2'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "</tr>";

		tableContainerHTML += "<tr id='footer-block'>";
		tableContainerHTML += "<td colspan='2'>";
		tableContainerHTML += "</td>";
		tableContainerHTML += "</tr>";
		tableContainerHTML += "</table>";

		var inforLogoHTML = "<img id='infor-logo' src='images/infor.jpg' />";

		var mainTitleHTML = "<div class='row'>";
		mainTitleHTML += "<div class='col-md-12 align-right'><span class='header1'>" + $mainTitle[0].value + "</span></div>";
		mainTitleHTML += "</div>";		
		
		var subHeaderHTML = "<div class='row'>";
		subHeaderHTML += "<div class='col-md-12 align-right'><span class='header2'>" + $subHeader[0].value + "</span></div>";
		subHeaderHTML += "</div>";		

		var coverImageHTML = "";
		if(isImageCropped) {
			var timestamp = new Date();	    
			coverImageHTML = "<img id='cover-image' src='images/crops/crop.png?t=" + timestamp.getTime() + "' />";
		} else {
			coverImageHTML = "<img id='cover-image' src='images/sample2.jpg' />";
		}

		var contentHeaderHTML = "";
		var mainContentHTML = "";

		var len = $mainContent.length;
		for(var i = 0; i < len; i++) {
			if(i > 0 && len >= 2) {
				mainContentHTML += "<br />____________________________________________________________________________"
			}

			mainContentHTML += "<h3>" + $contentHeader[i].value + "</h3>";
			mainContentHTML += $mainContent[i].value;
		}		

		var footerHTML = "NOTE: All information in this email is intended for internal communication only and should not be used or distributed outside of the company";

		$previewArea.append(tableContainerHTML);

		var $teamBlock = $("#team-block>td");
		var hasTeamText = false;

		len = teamArray.length;
		for(var i = 0; i < len; i++) {
			var team = teamArray[i];

			var cb = $("#" + team.id + "-cb")[0];

			var isChecked = cb.checked;
			var img = cb.attributes["team-logo"].value;
			var id = cb.attributes["team-id"].value;

			if(isChecked) {
				if(!hasTeamText) {
					hasTeamText = true;
					var teamHTML = $teamText[0].value + "<br /><br />";
					$teamBlock.append(teamHTML);
				}

				/* TEMP STYLE */
				var tmpStyle = id === 'e-sports' ? 'height: 55px' : '';
                tmpStyle = tmpStyle === '' && id === 'multisports' ? 'height: 70px; padding: 0 0 0 100px' : tmpStyle;
				
				var $teamImg = "<img id='" + id + "' src='images/teams/" + img + "' />";

				$teamBlock.append($teamImg);
			} else {
				$("#" + id).remove();
			}
		}

		var $logoBlock = $("#title-block>td.logo");
		var $headerBlock = $("#title-block>td.headers");
		var $imageBlock = $("#image-block>td");
		var $contentBlock = $("#content-block>td");
		var $footerBlock = $("#footer-block>td");		

		$logoBlock.append(inforLogoHTML);
		$headerBlock.append(mainTitleHTML);
		$headerBlock.append(subHeaderHTML);
		$imageBlock.append(coverImageHTML);
		$contentBlock.append(contentHeaderHTML);
		$contentBlock.append(mainContentHTML);
		$footerBlock.append(footerHTML);
	}	

	function hideCropControls() {
		$cropControl.css({
			"visibility": "hidden"
		});
	}

	function loadData() {
		//showContent("home");

		generateSplash();
	}	

	function resetSplash() {
		$mainTitle.val("Main Title");
		$subHeader.val("Sub Header");
		$contentHeader.val("Content Header");		

		hideCropControls();
	}

	function setVariables() {
		$mainNav = $(".main-nav");	
		$homeNav = $("#home-nav");
		$previewNav = $("#preview-nav");
		// $emailNav = $("#email-nav");		
		$allContent = $(".cntnt");	
		$homeContent = $("#home-content");
		$previewContent = $("#preview-content");
		// $emailContent = $("#email-content");		
		$mainTitle = $("#main-title");
		$subHeader = $("#sub-header");
		$contentGroup = $("#content-group");
		$linkButton = $("#link-button");
		$addButton = $("#add-button");
		$contentHeader = $(".content-header");
		$mainContent = $(".main-content");
		$previewArea = $("#preview-area");	
		$sendButton = $("#send-button");
		$copyButton = $("#copy-button");
		$imageCropper = $("#image-cropper");
		$cropButton = $("#crop-button");
		$uploadButton = $("#upload-button");
		$cropControl = $(".crop-control");
		$teamText = $("#team-text");
		$addNewSplashButton = $("#add-new-splash");

		// var el = document.getElementById('image-cropper');
		// imageCropper = new Croppie(el, {
		// 	boundary: { width: 723, height: 341 },
		//     viewport: { width: 723, height: 141 },
		//     // boundary: { width: 300, height: 300 },
		//     showZoomer: false,
		//     enableOrientation: true
		// });
		// imageCropper.bind({
		//     url: 'images/sample.jpg'		    
		// });				
	}

	function showContent(key) {
		$allContent.hide();

		switch(key) {
			case "home":
				$homeContent.show();
				break;

			case "preview":				
				$previewContent.show();
				break;

			case "email":
				$emailContent.show();
				break;

			default:
				$homeContent.show();
				break;
		}
	}
	
	function showCropControls() {
		$cropControl.css({
			"visibility": "visible"
		});
	}

	function unbindEvents() {
		$mainNav.unbind();
		$copyButton.unbind();
		$(".form-control").unbind();
	 	$cropButton.unbind();
	 	$addButton.unbind();
	 	$(".remove-control").unbind();
	 	$("#uploadImage").unbind();
	}
	
	function uploadDataUrl(dataUrl) {
		var formData = new FormData();
		formData.append("dataUrl", dataUrl); 
		
		$.ajax({
			url: "php/upload-data-url.php",
			type: "POST",
			data:  formData,
			contentType: false,
			cache: false,
			processData:false,
			success: function(data) {
				if(data == 'invalid file') {
					$("#err").html("Invalid File !").fadeIn();
				} else {
					var timestamp = new Date();
			    	$("#cover-image").attr("src", "images/crops/crop.png?t=" + timestamp.getTime());		    
				}
			},
			error: function(e) 
			{
				$("#err").html(e).fadeIn();
			}          
		});
	}
});